-- FastQuarry
 
local r  = require('robot')
local cm = require('computer')
local depth = 0
-- check that we have enough energy to continue.
local checkEnergy = function()
  local goHome = function()
    for i=0,depth do
      while r.detectUp() do r.swingUp() end
      assert(r.up())
     end
    os.exit(1)
  end
  if cm.energy() < 200 then
    for i=1,3 do cm.beep() end
    local component = require('component')
    if next(component.list("generator")) then
      if r.inventorySize() == 0 then
        print("Low energy, returning to surface.") 
        goHome()
      end
      if next(component.list("inventory")) then
        for i=1,r.inventorySize() do
          r.select(i)
          local item = component.inventory_controller.getStackInInternalSlot(i)
          if item then
            if string.find(item.name, "coal") or string.find(item.name, "wood") or string.find(item.name, "plank") then
              if component.generator.insert(r.count()) then return end
            end
            if i == r.inventorySize() then goHome() end
          end
        end
      end
      if r.count(r.inventorySize()) == 0 then
        for i=1,r.inventorySize() do
          if component.generator.insert(r.count()) then
            return
          end
        end
      end
      select(r.inventorySize())
      component.generator.insert(r.count())
    else
      print("Low energy, returning to surface.")
      goHome()
    end
  end
end
 
-- go forward n times, breaking anything above below and infront of him
-- if n is nil then go forward once.
-- TODO: Fix problem when reaching bedrock.
local forward = function(n)
  for i=1,n or 1 do
    while r.detect()     and r.swing()     do end
    while r.detectUp()   and r.swingUp()   do end
    while r.detectDown() and r.swingDown() do end
       
    while not r.forward() do
      while r.detect() do r.swing() end
    end
  end
end
 
local turn = function(turnFn)
  turnFn()
  forward()
  turnFn()
end
 
local quarry_layer = function(length, width)
  for i=1,width-1 do
    -- dig out this line
    forward(length-1)
 
    -- alternate between turning right and left
    if i % 2 == 0
      then turn(r.turnLeft)  -- second iteration
      else turn(r.turnRight) -- first iteration
    end
  end
 
  -- on the last line don't turn to a new line
  forward(length-1)
 
  -- go back to the quarry_start
  if width % 2 == 0 then
    r.turnRight()
    forward(width-1)
    r.turnRight()
  else
    r.turnLeft()
    forward(width-1)
    r.turnLeft()
    forward(length-1)
    r.turnLeft()
    r.turnLeft()
  end
  checkEnergy()
end
 
-- do a quarry, this is the main method
local quarry = function(length, width)
  while true do
    quarry_layer(length, width)
 
    -- move down three blocks
    for i=1,3 do
      while r.detectDown() and r.swingDown() do end
      if not r.down() then
        print('Bedrock!')
        break
      end
      depth = depth + 1
    end
  end
 
  -- Go back to the surface
  for i=0,depth do
    while r.detectUp() do r.swingUp() end
    assert(r.up())
  end
end
 
local args = {...}
if #args ~= 2 then
  print('Usage: quarry <length> <width>')
  os.exit(1)
end
 
local length = assert(tonumber(args[1]), 'length must be a number')
local width  = assert(tonumber(args[2]), 'width must be a number')
 
quarry(length, width)
